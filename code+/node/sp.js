const saml2 = require('saml2-js');
const fs = require('fs');


//configure identity provider
const idp_options = {
  sso_login_url: process.env.SSO_LOGIN_URL,
  sso_logout_url: process.env.SSO_LOGOUT_URL,
  certificates: [fs.readFileSync("saml/idp.crt").toString()]
};
const idp = new saml2.IdentityProvider(idp_options);


// Create service provider
const sp_options = {
  entity_id: "http://codeplus_biochem2024.duke.edu",
  private_key: fs.readFileSync("saml/sp.key").toString(),
  certificate: fs.readFileSync("saml/sp.crt").toString(),
  assert_endpoint: "http://vcm-41371.vm.duke.edu:3000/saml/consume",
  notbefore_skew: parseInt(5)
};
const sp = new saml2.ServiceProvider(sp_options);


// Read SAML request and provide attributes 
exports.consume_saml = (req, res) => {
  const options = { request_body: req.body };
  const hour = 3600000
  sp.post_assert(idp, options, (err, saml_response) => {
    if (err != null) {console.log("SAML ERROR: ", err);}

    req.session.cookie.expires = new Date(Date.now() + hour)
    req.session.samlAttrs = saml_response.user.attributes
    console.log(req.session.samlAttrs)
    req.session.save()
    return "aoeuaoeu"
  });

}
