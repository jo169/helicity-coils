const express = require('express');
const bodyParser = require('body-parser');
const proxy = require('express-http-proxy');
const sp = require('./sp');
const session = require('express-session');


const app = express();
const port = 3000;

app.use(session({
  secret: 'notaRealSecret',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: process.env.HTTP_SECURE ? true : false  } //note this MUST be set to false in dev and true in prod, otherwise session reading/writing wont work
}))

//consume saml
app.post('/saml/consume', bodyParser.urlencoded({ extended: true }),  async (req,res) => {
  await sp.consume_saml(req, res)
  res.redirect("/lab/tree/Admin.ipynb")
});

app.get('/saml/show', (req, res) => {
    console.log(req.session.samlAttrs['urn:oid:0.9.2342.19200300.100.1.1'][0])
});

const fs = require('fs');
function getAuthorizedUserIds(filePath) {
    const fileData = fs.readFileSync(filePath, 'utf8');
    const userIds = fileData.split(/\r?\n/);
    return userIds;
}
const authorizedUserIds = getAuthorizedUserIds('authorized.txt');


app.use('/lab/tree/Admin.ipynb', (req, res, next) => {
  
  if(req.session.samlAttrs && req.session.samlAttrs['urn:oid:0.9.2342.19200300.100.1.1'][0] !== undefined && authorizedUserIds.includes(req.session.samlAttrs['urn:oid:0.9.2342.19200300.100.1.1'][0])){ //the cookie is not expired and samlAttrs is present
    next()
  }else{
    res.redirect("https://shib.oit.duke.edu/idp/profile/SAML2/Unsolicited/SSO?providerId=http://codeplus_biochem2024.duke.edu&shire=http://vcm-41371.vm.duke.edu:3000/saml/consume");
  }
})


app.use('/', proxy('http://notebook:8080'));

app.listen(port, () => {
 console.log(`Express app listening at http://vcm-41371.vm.duke.edu:${port}`);
});
